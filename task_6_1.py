from random import randint


a = int(input())
b = int(input())
n = int(input())
m = int(input())
max_el = randint(a, b)
min_el = randint(a, b)
summator = 0
summ_row = 0
matrix = []
for i in range(n):
    matrix.append([])
    for j in range(m):
        element = randint(a, b)
        matrix[i].append(element)
        if element >= max_el:
            max_el = element
        elif element <= min_el:
            min_el = element
        summator += element
for row in matrix:
    print(row)
print(max_el)
print(min_el)
print(summator)
